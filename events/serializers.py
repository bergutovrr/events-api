from rest_framework import serializers

from services.serializers import ReadableRoomSerializer
from users.serializers import LightUserSerializer, UserWithEventsSerializer
from .models import Event

ALL = '__all__'


class EventListSerializer(serializers.ModelSerializer):
    owner = LightUserSerializer(many=False)
    subscriptions_count = serializers.IntegerField()
    subscription_id = serializers.IntegerField(default=None)
    room = ReadableRoomSerializer()

    class Meta:
        model = Event
        fields = ['id', 'owner', 'title', 'datetime', 'image', 'category', 'active', 'room',
                  "subscriptions_count", "subscription_id", 'duration']


class EventDetailSerializer(serializers.ModelSerializer):
    owner = UserWithEventsSerializer(many=False)
    subscriptions_count = serializers.IntegerField()
    subscription_id = serializers.IntegerField(default=None)
    room = ReadableRoomSerializer()

    class Meta:
        model = Event
        fields = ['id', 'owner', 'title', 'datetime', 'image', 'category', 'active', 'room',
                  "subscriptions_count", "subscription_id", 'duration']


class EventSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Event
        fields = ALL
