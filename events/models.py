import datetime

from django.db import models
from django.utils import timezone
from django.conf import settings

# Create your models here.
from core.abstract import EntityModel


class Event(EntityModel):
    PARTY = 'PRTY'
    SEMINAR = 'SMNR'
    FORUM = 'FRM'

    CATEGORY = (
        (PARTY, 'Вечеринка'),
        (SEMINAR, 'Семинар'),
        (FORUM, 'Форум')
    )

    title = models.CharField(max_length=255, verbose_name="Наименование")
    datetime = models.DateTimeField(verbose_name="Дата проведения")
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Организатор", related_name="events",
                              on_delete=models.CASCADE)
    category = models.CharField(verbose_name="Категория", choices=CATEGORY, max_length=10)
    published = models.BooleanField(verbose_name="Опубликовано", default=False)
    image = models.ImageField(verbose_name="Изображение", null=True, blank=True)
    duration = models.IntegerField(verbose_name="Примерная продолжительность в часах", default=1)
    room = models.ForeignKey('services.Room', verbose_name="Помещение", on_delete=models.CASCADE)

    class Meta:
        indexes = [
            models.Index(fields=['published', 'datetime']),
            models.Index(fields=['published']),
        ]
        ordering = ['datetime']
        verbose_name = "Мероприятие"
        verbose_name_plural = "Мероприятия"

    @property
    def active(self):
        end_datetime = self.datetime + datetime.timedelta(hours=self.duration)
        return self.datetime < timezone.now() < end_datetime

    def __str__(self):
        return "{} {} {}".format(self.datetime, self.title, self.owner)
