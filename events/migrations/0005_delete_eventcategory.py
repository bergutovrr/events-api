# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-21 05:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_auto_20171220_2143'),
    ]

    operations = [
        migrations.DeleteModel(
            name='EventCategory',
        ),
    ]
