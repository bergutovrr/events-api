import collections
import json
import hashlib

from django.core.cache import cache

from rest_framework.response import Response


def cache_view_set(timeout):
    def decorator(func):
        def inner(viewset, *args, **kwargs):
            request = viewset.request

            if request.method != "GET":
                return func(viewset, *args, **kwargs)

            query = request.query_params.copy()

            flush = query.pop('flush', None)

            query = collections.OrderedDict(sorted(query.items()))

            query = json.dumps(query)

            path = request.path

            cache_key = hashlib.md5((path + query).encode("UTF-8")).hexdigest()

            if flush is not None:
                cache.delete(cache_key)

            cached_result = cache.get(cache_key, None)

            if cached_result is None:
                response = func(viewset, *args, **kwargs)
                cache.set(cache_key, response.data, timeout)

            else:
                response = Response(cached_result)

            return response

        return inner

    return decorator
