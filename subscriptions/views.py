from django.shortcuts import render

from rest_framework import viewsets, permissions

from .models import Subscription
from .serializers import SubscriptionHydralizedSerializer, SubscriptionSerializer


# Create your views here.
class SubscriptionViewSet(viewsets.ModelViewSet):
    queryset = Subscription.objects.all()
    permission_classes = [permissions.DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = SubscriptionSerializer
    filter_fields = ['user', 'event']

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return SubscriptionSerializer

        return SubscriptionHydralizedSerializer
