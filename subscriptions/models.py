from django.db import models
from django.conf import settings

from events.models import Event


# Create your models here.
class Subscription(models.Model):
    event = models.ForeignKey(Event, verbose_name="Мероприятие", on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Пользователь", on_delete=models.CASCADE)

    class Meta:
        unique_together = ['event', 'user']
        verbose_name = "Подписка"
        verbose_name_plural = "Подписки"

    def __str__(self):
        return self.event.title + " " + self.user.username
