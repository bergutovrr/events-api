from rest_framework import serializers

from maps.models import City, Country
from users.models import User

from .models import Room, Service

ALL = '__all__'


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ['id', 'title']


class CitySerializer(serializers.ModelSerializer):
    country = CountrySerializer()

    class Meta:
        model = City
        fields = ['id', 'title', 'country']


class ReadableRoomSerializer(serializers.ModelSerializer):
    city = CitySerializer()

    class Meta:
        model = Room
        fields = ['id', 'title', 'address', 'capacity', 'city']


class WritableRoomSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Room
        fields = ALL


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email', 'username']


class ReadableServiceSerializer(serializers.ModelSerializer):
    owner = UserSerializer()
    category = serializers.SerializerMethodField()

    class Meta:
        model = Service
        fields = ['id', 'owner', 'category', 'price']

    def get_category(self, obj: Service) -> dict:
        return {
            'id': obj.category,
            'title': obj.get_category_display()
        }


class WritableServiceSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Service
        fields = ALL
