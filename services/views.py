from rest_framework import viewsets, permissions

# Create your views here.
from core.permissions import IsOwnerOrReadOnly

from .models import Room, Service
from .serializers import ReadableRoomSerializer, WritableRoomSerializer, ReadableServiceSerializer, \
    WritableServiceSerializer


class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all()
    permission_classes = [IsOwnerOrReadOnly]

    def get_serializer_class(self):
        if self.request.method in permissions.SAFE_METHODS:
            return ReadableRoomSerializer

        return WritableRoomSerializer


class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    permission_classes = [IsOwnerOrReadOnly]

    def get_serializer_class(self):
        if self.request.method in permissions.SAFE_METHODS:
            return ReadableServiceSerializer

        return WritableServiceSerializer
