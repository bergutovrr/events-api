from django.db.models import Count

from rest_framework import viewsets, permissions

from core.decorators import cache_view_set
from core.permissions import IsAdminOrReadOnly

from .models import Country, City
from .serializers import CountryCitySerializer, LightCountrySerializer, CitySerializer, WritableCitySerializer


# Create your views here.
class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    pagination_class = None
    permission_classes = [IsAdminOrReadOnly]

    def get_queryset(self):
        if self.request.method == "GET":
            return self.queryset.prefetch_related('city_set').annotate(cities_count=Count('city')).filter(
                cities_count__gt=0)

        return self.queryset

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CountryCitySerializer

        return LightCountrySerializer


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    permission_classes = [IsAdminOrReadOnly]
    pagination_class = None

    @cache_view_set(timeout=60 * 30)
    def list(self, request, *args, **kwargs):
        return super(CityViewSet, self).list(request, *args, **kwargs)

    def get_serializer_class(self):
        if self.request.method in permissions.SAFE_METHODS:
            return CitySerializer

        return WritableCitySerializer
