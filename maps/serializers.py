from rest_framework import serializers

from .models import Country, City

ALL = "__all__"


class CountrySerializer(serializers.ModelSerializer):
    """
    Базовый сериализатор для класса Country
    """

    class Meta:
        model = Country
        fields = ALL


class WritableCitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ALL


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ['id', 'title']


class LightCountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ['id', 'title']


class LightCitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ['id', 'title']


class CountryCitySerializer(serializers.ModelSerializer):
    cities = LightCitySerializer(source='city_set', many=True)

    class Meta:
        model = Country
        fields = ['id', 'title', 'cities']
