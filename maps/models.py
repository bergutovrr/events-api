from django.db import models

# Create your models here.
from core.abstract import EntityModel


class Country(EntityModel):
    title = models.CharField(max_length=255, verbose_name="Наименование")

    class Meta:
        verbose_name = "Страна"
        verbose_name_plural = "Страны"

    def __str__(self):
        return self.title


class City(EntityModel):
    title = models.CharField(max_length=255, verbose_name="Наименование")
    country = models.ForeignKey(Country, verbose_name="Страна", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Город"
        verbose_name_plural = "Города"

    def __str__(self):
        return self.title
